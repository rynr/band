import { Component, OnInit } from "@angular/core";
import { AsyncPipe } from "@angular/common";

import { Store } from "@ngrx/store";
import { Observable } from "rxjs";

import { Band } from "./entity/band/band.model";
import { selectBand } from "./entity/band/band.selectors";
import { SongActions } from "./entity/songs/song.actions";

@Component({
  selector: 'app-home',
  template: `
  @if (band$ | async) {
    <div role="group">
      <button class="outline">Songs</button>
      <button class="outline">Gigs</button>
      <button class="outline">Texts</button>
      <button class="outline" disabled data-tooltip="Yo are not authorized for the Administration">Administration</button>
    </div>
  } @else {
    <div role="group">
      <button class="outline" disabled data-tooltip="Please select a Band first">Songs</button>
      <button class="outline" disabled data-tooltip="Please select a Band first">Gigs</button>
      <button class="outline" disabled data-tooltip="Please select a Band first">Texts</button>
      <button class="outline" disabled data-tooltip="Please select a Band first">Administration</button>
    </div>
  }
  `,
  imports: [
    AsyncPipe
  ]
})
export class HomeComponent implements OnInit {
  readonly band$: Observable<Band | undefined>;

  constructor(private readonly store: Store) {
    this.band$ = store.select(selectBand);
  }

  ngOnInit(): void {
    this.band$.subscribe(
      (band) => {
        if (band) {
          this.store.dispatch(SongActions.requestSongList({ band: band }))
        }
      }
    );
  }
}