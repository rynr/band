import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { HeaderComponent } from "./header.component";

@Component({
  selector: 'app-root',
  imports: [RouterOutlet, HeaderComponent],
  template: `
  <div class="container">
    <app-header></app-header>
    <main>
      <router-outlet />
    </main>
    <footer></footer>
  </div>
  `,
  styles: [],
})
export class AppComponent {
  title = 'band';
}
