import { Routes } from '@angular/router';

import { HomeComponent } from './home.component';
import { BandComponent } from './page/band.component';

export const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'band/:id', component: BandComponent }
];
