import {createActionGroup, props} from "@ngrx/store";

import {Song} from "./song.model";
import {Band} from "../band/band.model";

export const SongActions = createActionGroup({
    source: 'Song',
    events: {
        'Request Song List': props<{ band: Band }>(),
    }
});

export const SongApiActions = createActionGroup({
    source: 'Song API',
    events: {
        'Retrieve Song List': props<{ band: Band, songs: Array<Song> }>()
    }
});