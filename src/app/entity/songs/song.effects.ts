import { Injectable } from "@angular/core";

import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, EMPTY, exhaustMap, map, Observable } from "rxjs";

import { Band } from "../band/band.model";
import { SongActions, SongApiActions } from "./song.actions";
import { Song } from "./song.model";
import { SongService } from "./song.service";

@Injectable()
export class SongEffects {
    private readonly getSongs$: Observable<{ band: Band, songs: Array<Song>}>;

    constructor(
        actions$: Actions,
        songService: SongService
    ) {
        this.getSongs$ = createEffect(() => actions$.pipe(
            ofType(SongActions.requestSongList),
            exhaustMap((action) => songService.getSongs(action.band)
                .pipe(
                    map(songs => (SongApiActions.retrieveSongList({ band: action.band, songs }))),
                    catchError(() => EMPTY)
                )
            )
        ));
    }
}

