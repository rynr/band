import {createReducer, on} from "@ngrx/store";

import {SongStore} from "./song.store";
import {SongActions, SongApiActions} from "./song.actions";

export const initialState: SongStore = {
    songs: [],
    loading: false
};

export const songReducer = createReducer(
    initialState,
    on(SongActions.requestSongList, (state, {band}) => ({...state, loading: true})),
    on(SongApiActions.retrieveSongList, (state, {band, songs}) => ({...state, band, songs, loading: false}))
)
