import { createFeatureSelector, createSelector } from "@ngrx/store";

import { SongStore } from "./song.store";

export const selectSongStore = createFeatureSelector<SongStore>('song')
export const selectSongs = createSelector(selectSongStore, (state: SongStore) => state.songs);
export const selectLoading = createSelector(selectSongStore, (state: SongStore) => state.loading);
