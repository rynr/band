export interface Song {
    id: string;
    artist: string;
    title: string;
    bpm: number | undefined;
    key: string | undefined;
}