import { HttpClient } from "@angular/common/http";
import { Band } from "../band/band.model";
import { Observable, of } from "rxjs";
import { Song } from "./song.model";
import { Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class SongService {
    private readonly baseUrl = '/api/song'
    constructor(private readonly http: HttpClient) { }

    getSongs(band: Band): Observable<Array<Song>> {
        return of([
            { id: '669bdbf8-1832-497c-b2d0-9c2cd0fa8845', title: `Song 1 for ${band.name}`, artist: band.name } as Song,
            { id: '0e5bb7b4-cbe1-4801-8084-c13a514b834c', title: `Song 2 for ${band.name}`, artist: band.name } as Song,
            { id: 'da2e4ad1-cb26-4e95-8b70-adcb201eb78b', title: `Song 3 for ${band.name}`, artist: band.name } as Song
        ]);
    }
}