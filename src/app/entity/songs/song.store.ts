import {Band} from "../band/band.model";
import {Song} from "./song.model";

export interface SongStore {
    band?: Band;
    songs: Array<Song>;
    loading: boolean;
}