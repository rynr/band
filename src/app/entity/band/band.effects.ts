import { Injectable } from "@angular/core";

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, EMPTY, map, mergeMap, Observable } from "rxjs";

import { BandActions, BandApiActions } from "./band.actions";
import { BandService } from "./band.service";
import { Band } from "./band.model";

@Injectable()
export class BandEffects {

    private readonly getBands$: Observable<{bands: Array<Band>}>;

    constructor(
        actions$: Actions,
        bandService: BandService
    ) {
        this.getBands$ = createEffect(() => actions$.pipe(
            ofType(BandActions.requestBandList),
            mergeMap(() => bandService.getBands()
                .pipe(
                    map(bands => (BandApiActions.retrieveBandList({ bands })),
                        catchError(() => EMPTY)
                    )
                )
            )
        ));
    }
}