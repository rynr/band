import { createReducer, on } from "@ngrx/store";

import { BandActions, BandApiActions } from "./band.actions";
import { BandStore } from "./band.store";

export const initialState: BandStore = {
    loading: false,
    bands: []
};

export const bandReducer = createReducer(
    initialState,
    on(BandActions.setBand, (state, { bandId }) => ({ ...state, band: state.bands.find(band => band.id === bandId) })),
    on(BandActions.requestBandList, (state) => ({ ...state, loading: true })),
    on(BandApiActions.retrieveBandList, (state, { bands }) => ({ ...state, loading: false, bands }))
)
