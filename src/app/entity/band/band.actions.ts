import {createActionGroup, emptyProps, props} from "@ngrx/store";

import {Band} from "./band.model";

export const BandActions = createActionGroup({
    source: 'Band',
    events: {
        'Set Band': props<{ bandId: string }>(),
        'Request Band List': emptyProps(),
    }
});

export const BandApiActions = createActionGroup({
    source: 'Band API',
    events: {
        'Retrieve Band List': props<{ bands: Array<Band> }>()
    }
});