import { createFeatureSelector, createSelector } from "@ngrx/store";

import { BandStore } from "./band.store";

export const selectBandStore = createFeatureSelector<BandStore>('band')
export const selectLoading = createSelector(selectBandStore, (state: BandStore) => state.loading);
export const selectBand = createSelector(selectBandStore, (state: BandStore) => state.band);
export const selectBands = createSelector(selectBandStore, (state: BandStore) => state.bands);