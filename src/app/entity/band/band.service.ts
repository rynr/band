import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { delay, Observable, of, throwError } from "rxjs";

import { Band } from "./band.model";

@Injectable({ providedIn: 'root' })
export class BandService {
    private readonly baseUrl = '/api/band'
    private readonly knownBands: Array<Band> = [
        { id: '669bdbf8-1832-497c-b2d0-9c2cd0fa8845', name: 'DullCovers!' },
        { id: '0e5bb7b4-cbe1-4801-8084-c13a514b834c', name: 'UnderCover Munich' },
        { id: 'da2e4ad1-cb26-4e95-8b70-adcb201eb78b', name: 'Wild Deuces' }
    ];
    constructor(private readonly http: HttpClient) { }

    getBands(): Observable<Array<Band>> {
        return of(this.knownBands).pipe(delay(1200));
    }

    getBand(id: string): Observable<Band> {
        let result = this.knownBands.find(band => band.id === id);
        if (result) {
            return of(result);
        } else {
            throw throwError(() => `Could not find Band with ID ${id}`);
        }
    }
}