import { Band } from "./band.model";

export interface BandStore {
    loading: boolean;
    band?: Band;
    bands: Array<Band>;
}