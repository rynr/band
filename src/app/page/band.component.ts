import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { Store } from "@ngrx/store";

import { BandActions } from "../entity/band/band.actions";

@Component({
  selector: "app-band",
  template: ` <div>Hello Band</div> `,
})
export class BandComponent {
  constructor(route: ActivatedRoute, store: Store) {
    route.paramMap.subscribe((params) => {
      const bandId = params.get("id");
      if (bandId) {
        store.dispatch(BandActions.setBand({ bandId }));
      }
    });
  }
}
