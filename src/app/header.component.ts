import { AsyncPipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";

import { Observable } from "rxjs";
import { Store } from "@ngrx/store";

import { BandActions } from "./entity/band/band.actions";
import { Band } from "./entity/band/band.model";
import { selectBand, selectBands, selectLoading } from "./entity/band/band.selectors";
import { Router } from "@angular/router";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  imports: [AsyncPipe],
})
export class HeaderComponent implements OnInit {
  readonly loading$: Observable<boolean>;
  readonly band$: Observable<Band | undefined>;
  readonly bands$: Observable<Array<Band>>;

  constructor(private readonly store: Store, private router: Router) {
    this.loading$ = store.select(selectLoading);
    this.bands$ = store.select(selectBands);
    this.band$ = store.select(selectBand);
  }

  ngOnInit(): void {
    this.store.dispatch(BandActions.requestBandList());
  }

  select(band: Band) {
    this.router.navigate(["band", band.id]);
  }
}
